terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "eu-west-3"
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_key_pair" "fedora_master_key" {
  key_name   = "fedora_master_key"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = ""
    cidr_blocks = []
  }

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "allow_all_outbound" {
  name        = "allow_all_outbound"
  description = "Allow ALL outbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = []
  }

  egress {
    description = "ALL outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_all_outbound"
  }
}

resource "aws_security_group" "allow_custom_inbound" {
  name        = "allow_custom_inbound"
  description = "Allow custom inbound"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "allow inbound from port 8000 to port 9000"
    from_port   = 8000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {

    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = []
  }

  tags = {
    Name = "allow_custom_inbound"
  }
}

resource "aws_security_group" "allow_cockpit" {
  name        = "allow_cockpit"
  description = "Allow cockpit inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "cockpit port"
    from_port   = 9090
    to_port     = 9090
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = ""
    cidr_blocks = []
  }

  tags = {
    Name = "allow_cockpit"
  }
}

resource "aws_security_group" "allow_https" {
  name        = "allow_https"
  description = "Allow https inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "https port"
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = ""
    cidr_blocks = []
  }

  tags = {
    Name = "allow_https"
  }
}



resource "aws_instance" "server" {
  ami           = "ami-0aeece425e2a782a0"
  instance_type = "t2.micro"
  vpc_security_group_ids = [
    aws_security_group.allow_all_outbound.id,
    aws_security_group.allow_custom_inbound.id,
    aws_security_group.allow_cockpit.id,
    aws_security_group.allow_ssh.id,
    aws_security_group.allow_https.id
  ]
  key_name = aws_key_pair.fedora_master_key.key_name

  tags = {
    Name = "Fedora - TF - Test"
  }
}

output "instance_ip_addr" {
  value = aws_instance.server.public_ip
}
