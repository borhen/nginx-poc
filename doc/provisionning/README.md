# Provisonning

- [Provisonning](#provisonning)
  - [Architecture de la solution](#architecture-de-la-solution)
  - [Environnement](#environnement)
  - [Terraform](#terraform)
    - [Requirements](#requirements)
    - [Commandes](#commandes)
      - [terraform init](#terraform-init)
      - [terraform plan](#terraform-plan)
      - [terraform apply](#terraform-apply)
  - [Ansible](#ansible)
    - [playbook tasks](#playbook-tasks)
    - [output](#output)

## Architecture de la solution
![](./images/full_solution_arch.png)

## Environnement
- Machine master dans aws ec2:
  - taille: t2.micro
  - ami: ami-0aeece425e2a782a0
  - système d'exploitation: fedora workstation 33

## Terraform
### Requirements
1. Code de la solution: [main.tf](../../terraform/main.tf)
2. terraform installé
3. client aws:
   1. Installer
   2. Dans aws IAM, créer un utilisateur qui a les permissions de gérer les instances ec2 (`AmazonECS_FullAccess`).
   3. Configurer pour utiliser les coordonnés de cet utilisateur pour s'authentifier.

### Commandes
#### terraform init
```
[fedora@ip-172-31-11-5 terraform]$ terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/aws versions matching "~> 3.0"...
- Installing hashicorp/aws v3.22.0...
- Installed hashicorp/aws v3.22.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

#### terraform plan
```
[fedora@ip-172-31-11-5 terraform]$ terraform plan

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.server will be created
  + resource "aws_instance" "server" {
      <...>
    }

  # aws_key_pair.fedora_master_key will be created
  + resource "aws_key_pair" "fedora_master_key" {
      <...>
    }

  # aws_security_group.allow_all_outbound will be created
  + resource "aws_security_group" "allow_all_outbound" {
      <...>
    }

  # aws_security_group.allow_cockpit will be created
  + resource "aws_security_group" "allow_cockpit" {
      <...>
    }

  # aws_security_group.allow_custom_inbound will be created
  + resource "aws_security_group" "allow_custom_inbound" {
      <...>
    }

  # aws_security_group.allow_https will be created
  + resource "aws_security_group" "allow_https" {
      <...>
    }

  # aws_security_group.allow_ssh will be created
  + resource "aws_security_group" "allow_ssh" {
      <...>
    }

Plan: 7 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + instance_ip_addr = (known after apply)

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```

#### terraform apply
```
[fedora@ip-172-31-11-5 terraform]$ terraform apply -auto-approve
aws_security_group.allow_custom_inbound: Creating...
aws_security_group.allow_https: Creating...
aws_security_group.allow_all_outbound: Creating...
aws_security_group.allow_cockpit: Creating...
aws_key_pair.fedora_master_key: Creating...
aws_security_group.allow_ssh: Creating...
aws_key_pair.fedora_master_key: Creation complete after 1s [id=fedora_master_key]
aws_security_group.allow_https: Creation complete after 1s [id=sg-0c869749422c20e90]
aws_security_group.allow_all_outbound: Creation complete after 1s [id=sg-05dc6cb827225ab24]
aws_security_group.allow_ssh: Creation complete after 1s [id=sg-06cb529e55d276d36]
aws_security_group.allow_custom_inbound: Creation complete after 2s [id=sg-0d70311f2a3071c31]
aws_security_group.allow_cockpit: Creation complete after 2s [id=sg-003ab1890a8f963c7]
aws_instance.server: Creating...
aws_instance.server: Still creating... [10s elapsed]
aws_instance.server: Still creating... [20s elapsed]
aws_instance.server: Creation complete after 21s [id=i-02380eaac639b492f]

Apply complete! Resources: 7 added, 0 changed, 0 destroyed.

Outputs:

instance_ip_addr = "15.236.209.192"
```

## Ansible
### playbook tasks
Le playbook de Ansible va:
1. [configurer la mahine créer avec terraform](../../ansible/roles/demo_echo_request/tasks/00-configure_machine.yml)
   - configurer le mot de passe de l'utilisateur par défaut fedora
   - ajouter les hosts pour le serveur 01 et le serveur 02
   - mettre à jour toutes les paquets
2. [installer podman](../../ansible/roles/demo_echo_request/tasks/01-install_podman.yml)
    - installer le paquet podman
    - activer le service podman
    - installer l'outil podman-compose
3. [installer cockpit](../../ansible/roles/demo_echo_request/tasks/02-install_cockpit.yml)
    - installer les paquets de cockpit
    - changer le port d'écoute de cockpit de `9090` vers `443`
    - autoriser le port `9090` dans SElinux
    - activer le service cockpit
4. [lancer les conteneurs](../../ansible/roles/demo_echo_request/tasks/03-run_containers.yml)
   - login dans gitlab (registre privé des images docker)
   - copier le fichier docker compose vers la nouvelle machine ec2
   - utiliser `podman-compose` pour lancer les conteneurs

### output
command:
```
# using extra_vars.yaml file
ansible-playbook -i hosts.py  playbook.yml --extra-vars="@extra_vars.yaml"
# using explicit extra_vars
ansible-playbook -i hosts.py playbook.yml  --extra-vars=docker_registry_url=<registry_url> --extra-vars=docker_registry_username=<username>
```
L'output de l'exécution du ansible-playbook doit être similaire à ça:
```
[fedora@master ansible]$ ansible-playbook -i hosts.py playbook.yml  --extra-vars=docker_registry_url=<registry_url> --extra-vars=docker_registry_username=<username>
Docker registry PASSWORD: 
fedora node password: 
confirm fedora node password: 

PLAY [all] *****************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : include_tasks] ***********************************************************************************************************
included: /home/fedora/nginx-proxy/ansible/roles/demo_echo_request/tasks/00-configure_machine.yml for 35.180.31.9

TASK [demo_echo_request : Wait for system to become reachable] *************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : Gather facts for the first time] *****************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : set user fedora password] ************************************************************************************************
changed: [35.180.31.9]

TASK [demo_echo_request : add hosts] ***************************************************************************************************************
ok: [35.180.31.9] => (item=127.0.0.1 local-server-01)
ok: [35.180.31.9] => (item=127.0.0.1 local-server-02)

TASK [demo_echo_request : update all packages] *****************************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : include_tasks] ***********************************************************************************************************
included: /home/fedora/nginx-proxy/ansible/roles/demo_echo_request/tasks/01-install_podman.yml for 35.180.31.9

TASK [demo_echo_request : install podman] **********************************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : enable and start podman service] *****************************************************************************************
changed: [35.180.31.9]

TASK [demo_echo_request : install podman-compose using pip] ****************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : include_tasks] ***********************************************************************************************************
included: /home/fedora/nginx-proxy/ansible/roles/demo_echo_request/tasks/02-install_cockpit.yml for 35.180.31.9

TASK [demo_echo_request : install cockpit] *********************************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : change default listen port] **********************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : enable seport] ***********************************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : enable and start cockpit service] ****************************************************************************************
changed: [35.180.31.9] => (item=cockpit)
changed: [35.180.31.9] => (item=cockpit.socket)

TASK [demo_echo_request : include_tasks] ***********************************************************************************************************
included: /home/fedora/nginx-proxy/ansible/roles/demo_echo_request/tasks/03-run_containers.yml for 35.180.31.9

TASK [demo_echo_request : login to gitlab registry] ************************************************************************************************
changed: [35.180.31.9]

TASK [demo_echo_request : pull {{ item.name }} image] **********************************************************************************************
ok: [35.180.31.9] => (item={'name': 'echo-request', 'image': 'nginx-proxy/echo-request'})
ok: [35.180.31.9] => (item={'name': 'nginx-proxy', 'image': 'nginx-proxy/nginx-proxy'})

TASK [demo_echo_request : copy docker-compose.yaml file] *******************************************************************************************
ok: [35.180.31.9]

TASK [demo_echo_request : podman-compose] **********************************************************************************************************
changed: [35.180.31.9]

PLAY RECAP *****************************************************************************************************************************************
35.180.31.9                : ok=21   changed=5    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```