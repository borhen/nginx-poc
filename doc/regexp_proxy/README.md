# proxy_pass avec des expressions régulieres
> Le code de la solution a été changé. Le code de cette documentation n'est plus à jour avec le code de la solution

- [proxy_pass avec des expressions régulieres](#proxy_pass-avec-des-expressions-régulieres)
  - [Environnement de test](#environnement-de-test)
  - [Config sur la machine hôte](#config-sur-la-machine-hôte)
  - [Les serveurs golang](#les-serveurs-golang)
  - [nginx config](#nginx-config)
    - [nginx Dockerfile](#nginx-dockerfile)
    - [./conf/default.conf](#confdefaultconf)
  - [Résultat](#résultat)
    - [Expressions régulieres basiques](#expressions-régulieres-basiques)
      - [location ~^\/server-01$](#location-server-01)
      - [location ~^\/server-02$](#location-server-02)
    - [Expressions régulieres avec capture d'un bloc](#expressions-régulieres-avec-capture-dun-bloc)
      - [location ~^\/api\/v1\/server-01\/(?<path>.*)$](#location-apiv1server-01path)
      - [location ~^\/api\/v1\/server-02\/(?<path>.*)$](#location-apiv1server-02path)
    - [Expressions régulieres avec capture de plusieurs bloc](#expressions-régulieres-avec-capture-de-plusieurs-bloc)
      - [location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$](#location-apiv2server-serveridddpath)

## Environnement de test
![](./images/arch_solution.png)

L'environnement est composé de 3 parties:
| nom | role | port |
|---|---|---|
| postman | Simuler les requêtes des utilisateurs. |  |
| 2 serveurs golang | Écho le header des requêtes issues: | `8801` (host = `local-server-01`) et `8801` (host = `local-server-01`) |
| serveur nginx | Joue le rôle d'un proxy. Il "transfert" les requêtes vers le serveur golang et ajoute un header personnalisé `X-test-header : test-value`.  | `8080` |

- Les serveurs golang et nginx éxucter sous forme des conteneurs Docker. Pour faciliter la communication avec les conteneurs, j'ai utilisé le résau du hôte avec l'option `--net=host`.
- Pour gérer les conteneur, j'ai utiliser l'outil `podman` qui supporte toute les commandes `docker`.


## Config sur la machine hôte
``` bash
[ ]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

127.0.0.1 local-server-01
127.0.0.1 local-server-02
```

## Les serveurs golang
Pour exécuter le serveur golang, j'ai utilisé l'image docker `docker.io/docker.io/library/golang:1.15`

J'ai utilisé le code suivant pour le serveur golang:
[golang/main.go](../../golang/main.go)

Pour le serveur 01:
``` bash
[ ]$ export HOST=local-server-01
[ ]$ export PORT=8801
[ ]$ podman run --net=host --env=PORT=$PORT --env=HOST=$HOST -it -v $PWD/golang/main.go:/go/src/app/main.go:z --name=go-server-$PORT --rm golang:1.15 go run /go/src/app/main.go
2020/12/22 12:33:15 server listen on http://c:8801/
```
Pour le serveur 02:
``` bash
[ ]$ export HOST=local-server-02
[ ]$ export PORT=8802
[ ]$ podman run --net=host --env=PORT=$PORT --env=HOST=$HOST -it -v $PWD/golang/main.go:/go/src/app/main.go:z --name=go-server-$PORT --rm golang:1.15 go run /go/src/app/main.go
2020/12/22 12:35:47 server listen on http://local-server-02:8802/
```


## nginx config
### nginx Dockerfile
J'ai utilisé le fichier docker suivant pour créer l'image nginxM
```
[ ]$ cat Dockerfile 
FROM nginx:1.19.6

RUN echo '127.0.0.1 local-server-01' >> /etc/hosts
RUN echo '127.0.0.1 local-server-02' >> /etc/hosts

COPY ./conf/default.conf /etc/nginx/conf.d
[ ]$ podman build -f Dockerfile -t nginx-header .
STEP 1: FROM nginx:1.19.6
...
[ ]$ podman run --net=host --name nginx --rm nginx-header
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf differs from the packaged version
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
```

### ./conf/default.conf
Contenu du fichier `./conf/default.conf`:
``` conf
[ ]$ cat conf/default.conf
server {
    listen 8080;
    proxy_set_header X-test-header test-value;

    location ~^\/server-01$ {
        proxy_pass http://local-server-01:8801;
    }

    location ~^\/server-02$ {
        proxy_pass http://local-server-02:8802;
    }
    
    location ~^\/api\/v1\/server-01\/(?<path>.*)$ {
        proxy_pass http://local-server-01:8801/$path;
    }

    location ~^\/api\/v1\/server-02\/(?<path>.*)$ {
        proxy_pass http://local-server-02:8802/$path;
    }

    location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$ {
        proxy_pass http://local-server-$serverId:88$serverId/$path;
    }
}
```

## Résultat
### Expressions régulieres basiques
#### location ~^\/server-01$
![](./images/postman_server01.png)

#### location ~^\/server-02$
![](./images/postman_server02.png)

### Expressions régulieres avec capture d'un bloc
#### location ~^\/api\/v1\/server-01\/(?<path>.*)$
![](./images/postman_api_v1_server01.png)


#### location ~^\/api\/v1\/server-02\/(?<path>.*)$
![](./images/postman_api_v1_server02.png)

### Expressions régulieres avec capture de plusieurs bloc
#### location ~^\/api\/v2\/server-(?<serverId>\d\d)\/(?<path>.*)$
1. Requête vers le serveur 01:
![](./images/postman_api_v2_server01.png)


2. Requête vers le serveur 02:
![](./images/postman_api_v2_server02.png)
