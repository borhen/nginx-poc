#!/usr/bin/python

import subprocess
import json
import os


def get_inventory():
    os.chdir("../terraform")
    ip = subprocess.getoutput('terraform output -raw instance_ip_addr')
    return {"nodes": {"hosts": [ip], "vars":{"ansible_python_interpreter": "/usr/bin/python3", "host_key_checking": "False"}}}

print(json.dumps(get_inventory()))