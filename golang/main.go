package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"
)

const (
	// ID holds the default server ID.
	ID = "golang-server"
	// DefaultHost name of the HTTP Server
	DefaultHost = "0.0.0.0"
	// DefaultPort of the HTTP Server
	DefaultPort = "8888"
	// CustomHeaderName that have been added by nginx
	CustomHeaderName = "X-my-header"
)

var serverID, host, port string

func getSortedKeys(header http.Header) (keys []string) {
	for k := range header {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return
}

func logAndPrint(w http.ResponseWriter, s string) {
	fmt.Fprintf(w, s)
	log.Print(s)
}

func echo(w http.ResponseWriter, header http.Header) {
	for _, k := range getSortedKeys(header) {
		logAndPrint(w, fmt.Sprintf("%-25s:\t%s\n", k, strings.Join(header[k], ",")))
	}
}

func echoRequest(w http.ResponseWriter, r *http.Request) {
	logAndPrint(w, "--> Custom Params:\n")
	params := http.Header{
		"X-Server-ID":  []string{serverID},
		"X-Host":       []string{r.Host},
		"X-Path":       []string{r.URL.Path},
		"X-Time-Stamp": []string{time.Now().String()},
	}
	echo(w, params)
	logAndPrint(w, "--> Headers Params:\n")
	echo(w, r.Header)
}

func main() {
	// parse flags
	flag.StringVar(&serverID, "id", ID, fmt.Sprintf("Specify server ID. Default is %s.", ID))
	flag.StringVar(&port, "port", DefaultPort, fmt.Sprintf("Specify port. Default is %s.", DefaultPort))
	flag.StringVar(&host, "host", DefaultHost, fmt.Sprintf("Specify host. Default is %s.", DefaultHost))
	flag.Parse()

	// run server
	http.HandleFunc("/", echoRequest)
	log.Printf("server listen on http://%s:%s/", host, port)
	err := http.ListenAndServe(host+":"+port, nil)
	if err != nil {
		log.Fatal("Error Starting the HTTP Server :", err)
		return
	}
}
